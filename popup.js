document.addEventListener("DOMContentLoaded", () => {
  document.querySelector(".start").addEventListener("click", () => {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
      chrome.scripting.executeScript({
        target: { tabId: getTabId(tabs) },
        files: ["content.js"],
      });
    });
  });
});

function getTabId(tabs) {
  return tabs[0].id;
}
