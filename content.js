async function main() {
  await scrollToBottom();

  const products = await productsScraping();

  // const createProductsUrl = `https://shoppe-sg-scrape.vercel.app/api`;
  // await axios.post(createProductsUrl, {
  //   url: window.location.href,
  //   products: products,
  // });
  // alert("Success!");

  await xlsxExporter(products);
}

async function scrollToBottom() {
  return new Promise((resolve) => {
    var totalHeight = 0;
    var distance = 800;
    var timer = setInterval(() => {
      var scrollHeight = document.querySelector("#main")?.scrollHeight;
      window.scrollBy(0, distance);
      totalHeight += distance;

      if (totalHeight >= scrollHeight - window.innerHeight) {
        clearInterval(timer);
        resolve(true);
      }
    }, 300);
  });
}

async function xlsxExporter(products) {
  const ws = XLSX.utils.json_to_sheet(products, {
    header: ["商品名", "価格"],
  });
  const wb = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, "result");
  XLSX.writeFile(wb, `result_${Date.now()}.xlsx`);
}

async function productsScraping() {
  const rootUrl = window.location.href;
  const elementSelector = rootUrl.includes("shopee.sg/search")
    ? ".shopee-search-item-result__item"
    : ".shop-search-result-view__item";

  const products = Array.from(document.querySelectorAll(elementSelector)).map((item) => {
    const name = item?.querySelector("a > div > div:nth-child(2) > div > div")?.textContent;
    const price = item?.querySelector(
      "a > div > div:nth-child(2) > div:nth-child(2) > div span:nth-child(2)"
    )?.textContent;

    return {
      商品名: name,
      価格: price,
    };
  });

  return products;
}

main();
